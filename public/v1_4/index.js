
const on_click_download_links = (el, force_open) => {
    const parent = el.closest('div[id]');
    if (!parent) {
        return;
    }

    const slide_el = parent.querySelector('div.js-slide');
    if (!slide_el) {
        return;
    }

    const arrow_el = el.querySelector('.js-slide-arrow');
    if (!arrow_el) {
        return;
    }

    const arrow_content = arrow_el.textContent;

    arrow_el.textContent = force_open === undefined ? (arrow_content === '▲' ? '▼' : '▲') : (force_open === true ? '▲' : '▼');
    slide_el.classList.toggle('open', force_open);
}

const on_card_click = () => {
    const el = document.getElementById('manual_annotations');
    if (!el) {
        throw new Error('Manual annotations div not found!');
    }
    on_click_download_links(el, true);
}

const get_hover_gif_handlers = (el) => {
    const img = el.querySelector('img');
    if (!img) {
        return {
            on_mouseleave: null,
            on_mouseenter: null,
        }
    }
    const _src = img.src
    const _gifsrc = _src.slice(0, -3) + 'gif';
    let fetched = false;
    const on_mouseleave = () => {
        img.src = _src
        // img.classList.replace('object-contain', 'object-cover');
    }
    const set_gif = () => {
        img.src = _gifsrc;
        // img.classList.replace('object-cover', 'object-contain');
    }
    const on_mouseenter = () => {
        if (!fetched) {
            fetch(_gifsrc).then(() => {
                fetched = true;
                set_gif()
            });
        } else {
            set_gif()
        }
    }

    return {
        on_mouseleave,
        on_mouseenter
    }
}
const setup_slide_handlers = () => {
    const elements = document.querySelectorAll('.js-slide-control');

    for (const el of elements) {
       el.onclick = () => on_click_download_links(el);
       on_click_download_links(el, false);
    }
}

const setup_hover_gif = () => {
    const elements = document.querySelectorAll('.js-hover-gif');
    for (const el of elements) {
        const { on_mouseleave, on_mouseenter } = get_hover_gif_handlers(el);
        el.onmouseleave = on_mouseleave;
        el.onmouseenter = on_mouseenter;
    }
}

const setup_card_click = () => {
    const elements = document.querySelectorAll('.js-scroll-click');
    for(const el of elements) {
        el.onclick = on_card_click
    }
}


const setup_navbar = () => {
    const desktop_menu = document.getElementById('desktop-menu');
    const mobile_menu = document.getElementById('mobile-menu');
    const mobile_menu_button = document.getElementById('mobile-menu-button');
    const [open_menu, close_menu] = mobile_menu_button.querySelectorAll('svg');

    const on_link_click = (e) => {
        const el = e.target;
        const current = e.currentTarget.querySelector('a[aria-current="page"]');
        if (!(el instanceof HTMLAnchorElement) || el === current) {
            return;
        }

        current.removeAttribute('aria-current');
        el.setAttribute('aria-current', 'page');

        current.classList.add('text-gray-300', 'hover:bg-gray-700', 'hover:text-white');
        current.classList.remove('bg-gray-900','text-white');

        el.classList.remove('text-gray-300', 'hover:bg-gray-700', 'hover:text-white');
        el.classList.add('bg-gray-900','text-white');

        toggle_menu();

    }
    const toggle_menu = () => {
        const aria_expanded = (mobile_menu_button.getAttribute('aria-expanded') || false) === "true";
        const should_open = !aria_expanded;

        mobile_menu_button.setAttribute('aria-expanded', should_open);
        // Toggle menu icon
        if (should_open) {
            close_menu.classList.replace('hidden', 'block');
            open_menu.classList.replace('block', 'hidden');
            mobile_menu.classList.remove('hidden');
            mobile_menu.classList.add('block');
        } else {
            open_menu.classList.replace('hidden', 'block');
            close_menu.classList.replace('block', 'hidden');
            mobile_menu.classList.remove('block');
            mobile_menu.classList.add('hidden');   
        }
    }
    
    mobile_menu_button.onclick = toggle_menu;
    mobile_menu.onclick = on_link_click;
    desktop_menu.onclick = on_link_click;

}

const setup = () => {
    setup_slide_handlers();
    setup_hover_gif();
    setup_card_click();
    setup_navbar();
}


window.addEventListener('DOMContentLoaded', setup)